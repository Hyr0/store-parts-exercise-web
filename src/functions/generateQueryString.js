const generateQueryString = (filters = {}) => {
    let queryString = '';
    Object.keys(filters).forEach(key => {
        if (filters[key]) {
            queryString += `${!queryString.length ? '?' : '&'}${key}=${filters[key]}`;
        }
    })
    return queryString;
}
export default generateQueryString;