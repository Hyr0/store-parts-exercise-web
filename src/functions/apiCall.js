import generateQueryString from './generateQueryString';
import orderBySortOrder from './orderBySortOrder';
const APICall = ({ filters, path, sortOrder }) => {
    const params = {
        method: 'GET',
        redirect: 'follow'
    }
    let url = `http://localhost:8081/${path}${generateQueryString(filters)}`;
    return fetch(url, params)

        .then(response => response.json())
        .then(result => 
            sortOrder ? orderBySortOrder({ sortOrder, data: result }) : result
        )
}
export default APICall;