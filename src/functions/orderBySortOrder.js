import SortOrder from '../types/sortOrder';
const convertPrice = (value) => {
    return Number(value.replace('$', ''));
}
const sortArray = ({ sortOrder, data }) => {
    const { ASC } = SortOrder;
    if (sortOrder === ASC) {
        return data.sort((a, b) =>
            convertPrice(a.price) < convertPrice(b.price) ?
                -1 : (convertPrice(a.price) > convertPrice(b.price) ?
                    1 : 0))
    } else {
        return data.sort((a, b) =>
            convertPrice(b.price) < convertPrice(a.price) ?
                -1 : (convertPrice(b.price) > convertPrice(a.price) ?
                    1 : 0))
    }
}
export default sortArray;