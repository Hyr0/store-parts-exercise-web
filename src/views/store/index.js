import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import StoreList from './list'
import StoreDetail from './detail'
import {StoreProvider} from './states/storeContext';
const StoreRouter = () => {
    return (
        <StoreProvider>
            <Fragment>
                <Route exact path='/store-parts' component={StoreList} />
                <Route path='/store-parts/:id' component={StoreDetail} />
            </Fragment>
        </StoreProvider>
    )
}

export default StoreRouter;