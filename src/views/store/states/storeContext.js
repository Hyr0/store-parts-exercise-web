import { createContext, useEffect } from 'react'
import useFilterHook from './filterHook';
import useStorePartsHook from './storePartsHook';
import usePartsTypesHook from './partsTypesHook';
const StoreContext = createContext({});

export const StoreProvider = (props) => {
    const filterHook = useFilterHook();
    const storePartHook = useStorePartsHook();
    const partyTypes = usePartsTypesHook();
    const { filters, sortOrder } = filterHook;
    const { fetchPartsList } = storePartHook;
    const { children } = props;
    const factory = {
        ...filterHook,
        ...storePartHook,
        ...partyTypes,
        onSelectPart: (id) => console.log(id),
    }

    useEffect(() => {
        fetchPartsList({
            filters,
            sortOrder,
        })
    }, [fetchPartsList, filters, sortOrder])

    return <StoreContext.Provider
        value={factory}
    >
        {children}
    </StoreContext.Provider>;
}
export const StoreConsumer = StoreContext.Consumer;

export default StoreContext;