import { useEffect, useState } from 'react';
import APICall from '../../../functions/apiCall';
const StorePartsHook = () => {
    const [partTypes, setPartTypes] = useState([]);
    const fetchPartTypes = () => {
        APICall({
            path: 'store/part-types',
        })
            .then(result => setPartTypes(result))
            .catch(error => console.log('error', error));
    }
    useEffect(() => {
        fetchPartTypes()
    }, [])
    return { partTypes };
}
export default StorePartsHook;