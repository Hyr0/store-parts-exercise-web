import { useState } from 'react';
import APICall from '../../../functions/apiCall';
const StorePartsHook = () => {
    const [storeParts, setStoreParts] = useState([]);

    const fetchPartsList = ({ filters, sortOrder }) => {
        APICall({
            path: 'store/parts',
            filters,
            sortOrder,
        })
            .then(result => setStoreParts(result))
            .catch(error => console.log('error', error));
    }
    const fetchPartsDetail = (index) => {
        return storeParts[index];
    }

    return {
        storeParts,
        fetchPartsList,
        fetchPartsDetail
    }
}
export default StorePartsHook;