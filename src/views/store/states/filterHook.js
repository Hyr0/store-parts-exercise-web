import { useState } from 'react';
import SortOrder from '../../../types/sortOrder';
const FilterHook = () => {
    const { ASC, DESC } = SortOrder;
    const [sortOrder, setSortOrder] = useState(ASC);
    const [filters, setFilters] = useState({
        type: null,
        query: null,
    });
    const handleFilterChange = ({ key, value }) => {
        setFilters({ ...filters, [key]: value });
    }
    const handleSortOrderChange = () => {
        setSortOrder(sortOrder === ASC ? DESC : ASC);
    }
    return {
        filters,
        sortOrder,
        handleFilterChange,
        handleSortOrderChange,
    }
}
export default FilterHook;