import React, { useState, useContext } from 'react';
import SearchIcon from './searchIcon.png';
import SortIcon from './sortIcon.png';
import css from './styles.module.css'
import SortOrder from '../../../../types/sortOrder';
import StoreContext from '../../states/storeContext';
const FilterForm = () => {
    const storeContext = useContext(StoreContext);
    const { ASC } = SortOrder;
    const {
        sortOrder,
        partTypes,
        filters,
        handleSortOrderChange,
        handleFilterChange
    } = storeContext;
    const [query, setQuery] = useState(filters.query || '');
    const handleQueryChange = (e) => {
        const value = e.target.value;
        setQuery(value);
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        handleFilterChange({
            key: 'query',
            value: query.length ? query : null,
        })
    }
    const handlePartTypeChange = (e) => {
        const value = e.target.value;
        handleFilterChange({
            key: 'type',
            value: value.length ? value : null,
        })
    }
    return <form onSubmit={handleSubmit} className={css.form}>
        <div className={css.inputWrap}>
            <input
                type='text'
                placeholder='Search...'
                className={css.input}
                value={query}
                onChange={handleQueryChange}
            />
            <img
                alt='Search...'
                src={SearchIcon}
                className={`${css.icon} ${css.button}`}
                onClick={handleSubmit} />
        </div>
        <div className={css.inputWrap}>
            <select
                className={css.input}
                value={filters.type || ''}
                onChange={handlePartTypeChange}
            >
                <option value={''}>Select Type...</option>
                {partTypes.map(partType =>
                    <option
                        key={partType}
                        value={partType}>
                        {partType}
                    </option>
                )}
            </select>
        </div>
        <div
            className={`${css.inputWrap} ${css.button}`}
            onClick={handleSortOrderChange}>
            <p className={css.input}>Price Order</p>
            <img
                alt='Sort'
                src={SortIcon}
                className={css[sortOrder === ASC ? 'icon' : 'iconInverse']}
            />
        </div>
    </form>
};

export default FilterForm;