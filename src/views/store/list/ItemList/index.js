import React, { useContext } from 'react';
import css from './styles.module.css'
import StoreContext from '../../states/storeContext';
import { useHistory, useLocation } from 'react-router-dom';
const FilterForm = () => {
    const storeContext = useContext(StoreContext);
    const history = useHistory();
    const location = useLocation();
    const {
        storeParts,
    } = storeContext;
    const handleNavigation = (index) => {
        history.push(`${location.pathname}/${index}`)
    }
    return <div className={css.listWrap}>
        {storeParts.map((storePart, index) =>
            <div key={storePart.name} className={css.listItem} onClick={handleNavigation.bind(null, index)}>
                <p>{storePart.name}</p>
                <p>{storePart.type}</p>
                <p>{storePart.price}</p>
            </div>
        )}
    </div>
};

export default FilterForm;