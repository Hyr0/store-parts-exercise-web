import React from 'react';
import PageWrapper from '../../../components/PageContainer';
import Header from '../../../components/Header';
import FilterForm from './FilterForm';
import ItemList from './ItemList';
const StoreListView = () => {
    return (
        <PageWrapper>
            <Header text='Store Parts' />
            <FilterForm />
            <ItemList />
        </PageWrapper>
    )
};

export default StoreListView;