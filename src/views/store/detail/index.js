import React from 'react';
import PageWrapper from '../../../components/PageContainer';
import Header from '../../../components/Header';
import Item from './Item'
const StoreDetailView = () => {
    return (
        <PageWrapper>
            <Header text='Store Parts Detail' />
            <Item />
        </PageWrapper>
    )
};

export default StoreDetailView;