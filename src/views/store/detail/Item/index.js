import React, { useContext } from 'react';
import css from './styles.module.css'
import StoreContext from '../../states/storeContext';
import { useHistory } from 'react-router-dom';
const FilterForm = () => {
    const storeContext = useContext(StoreContext);
    const history = useHistory();
    const {
        fetchPartsDetail,
    } = storeContext;
    const storePartDetail = fetchPartsDetail(1);
    if (!storePartDetail) {
        history.push('/store-parts')
        return null;
    } else {
        return (
            <div className={css.itemWrap}>
                <p>Name: {storePartDetail.name}</p>
                <p>Type: {storePartDetail.type}</p>
                <p>Price: {storePartDetail.price}</p>
            </div>
        )
    }
};

export default FilterForm;