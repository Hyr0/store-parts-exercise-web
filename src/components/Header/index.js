import React from 'react';
import css from './styles.module.css'
const Header = ({ text }) => {
    return (
        <h3 className={css.title}>
            {text}
        </h3>
    )
};

export default Header;