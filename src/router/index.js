import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import Store from '../views/store';

const Router = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Store />
            </Switch>
        </BrowserRouter>
    )
}

export default Router;