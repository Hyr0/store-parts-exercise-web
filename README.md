## APP Documentation

To run the APP you will just need the basics.

```
yarn
yarn start
```

or

```
npm install
npm run start
```

You should be able to see the webpage on `http://localhost:3000`.

#### Requires the supplied API to be running.